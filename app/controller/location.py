# encoding: utf-8

from flask import g, render_template, request

from app.controller.controller import Controller
from app.model.location import LocationModel, delete_branch, insert_branch
from app.form.location import LocationNewForm


class Location(Controller):
    def index(self):
        g.tree = LocationModel.query.filter_by(level=0).all()
        g.locations = LocationModel.query.order_by(LocationModel.left_index).all()
        g.form = LocationNewForm()
        choices = [(location.id, location.name) for location in g.locations]
        choices.insert(0, (0, ""))
        g.form.parent.choices = choices
        if request.method == "POST":
            if g.form.validate_on_submit():
                # Creating new branch
                branch = [
                    {
                        "name": g.form.name.data,
                        "parent_id": g.form.parent.data if g.form.parent.data > 0 else None,
                        "level": 0,
                        "left_index": 0,
                        "right_index": 1,
                    },
                ]
                insert_branch(branch, LocationModel.query.get(branch[0]["parent_id"]))
                g.locations = LocationModel.query.order_by(LocationModel.left_index).all()
            else:
                print(g.form.errors)
            return render_template("location/list.html")
        return render_template("location/index.html")

    def edit(self, location_id):
        location = LocationModel.query.get(location_id)
        if request.method == "DELETE":
            delete_branch(location.left_index, location.right_index)
            return ""
