# encoding: utf-8

from flask import g, render_template, request

from app.controller.controller import Controller
from app.model.equipment import EquipmentModel
from app.form.equipment import EquipmentNewForm


class Equipment(Controller):
    def index(self):
        g.equipments = EquipmentModel.query.all()
        g.form = EquipmentNewForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                equipment = EquipmentModel(
                    code=g.form.code.data,
                    short_name=g.form.short_name.data,
                    long_name=g.form.long_name.data,
                )
                equipment.save()
                g.equipments.append(equipment)
            else:
                print(g.form.errors)
            return render_template("equipment/list.html")
        return render_template("equipment/index.html")

    def edit(self, equipment_id):
        equipment = EquipmentModel.query.get(equipment_id)
        if request.method == "DELETE":
            equipment.delete()
            return ""
