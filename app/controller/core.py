# encoding: utf-8

from flask import g, render_template, request
from sqlalchemy import func

from app.controller.controller import Controller
from app.model.location import LocationModel
from app.model.equipment import EquipmentModel
from app.model.asset import AssetModel


class Core(Controller):
    def home(self):
        return render_template("core/home.html")

    def login(self):
        return render_template("core/login.html")

    def test(self):
        location_id = None

        # On récupère les locations possibles
        query = LocationModel.query
        if location_id is not None:
            query = query.filter_by(id=location_id)
        g.locations = query.order_by(LocationModel.left_index).all()

        # On récupère les équipements
        query = EquipmentModel.query
        if location_id is not None:
            query = query.join(EquipmentModel.assets).join(AssetModel.location).filter_by(id=location_id)
        g.equipments = query.all()

        # On récupère les actifs connus aussi
        query = AssetModel.query
        if location_id is not None:
            query = query.join(AssetModel.location).filter_by(id=location_id)
#        else:
#            query = query(func.sum(AssetModel.quantity)).group_by(AssetModel.equipment_id)
        g.assets = query.all()

        # On affiche tout ça
        return render_template("core/test.html")
