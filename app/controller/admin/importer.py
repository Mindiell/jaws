# encoding: utf-8

import csv
from io import StringIO

from flask import g, request

from app.form.admin import ImportForm


def default_import(headers):
    g.form = ImportForm()
    g.errors = []
    g.messages = []
    reader = None
    if g.form.validate_on_submit():
        reader = csv.reader(
            StringIO(request.files["filename"].read().decode("utf-8"))
        )
        for index, row in enumerate(next(reader, [])):
            for header in headers:
                if row.lower() == header:
                    headers[header] = index
        for header in headers:
            if headers[header] is None:
                g.errors.append(f"Column {header} not found.")
        for header in headers:
            if headers[header] == "optional":
                headers[header] = None

    return reader
