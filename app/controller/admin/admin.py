# encoding: utf-8

from flask import g, redirect, url_for, flash, session
from flask_login import login_user, logout_user
from flask_admin import AdminIndexView, BaseView, expose

from app import admin
from app.controller.controller import Controller
from app.form.user import UserLoginForm
from app.model.user import UserModel


class LogoutView(BaseView):
    @expose('/')
    def index(self):
        logout_user()
        session.clear()
        return redirect(url_for('admin.index'))

#TODO: Display this link in Admin menu only if user is authenticated
admin.add_view(LogoutView(name='Logout', endpoint='logout'))