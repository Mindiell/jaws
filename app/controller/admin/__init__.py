# encoding: utf-8

from app.controller.admin.supplier import ImportSupplierView


admin_routes = (
    (ImportSupplierView, "Suppliers", "suppliers", "Import"),
)
