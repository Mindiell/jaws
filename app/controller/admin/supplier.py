# encoding: utf-8

from flask import g
from flask_admin import BaseView, expose

from app.controller.admin.importer import default_import
from app.form.admin import ImportForm
from app.model.supplier import SupplierModel


class ImportSupplierView(BaseView):
    @expose("/", methods=["GET", "POST"])
    def index(self):
        headers = {
            "code": None,
            "name": None,
            "address": "optional",
            "phone": "optional",
            "website": "optional",
            "email": "optional",
        }
        reader = default_import(headers)
        g.what = {
            "title": "Import suppliers",
            "description": "Importing suppliers will add unknown ones and update known"
                " ones. If a supplier is not present in imported file it will not be"
                " deleted.",
            "endpoint": "suppliers.index",
            "formats": [
                "File format accepted is CSV (Comma Separated Values).",
                "First line chould be column headers.",
                "Other lines are values.",
                "One column MUST be 'code' and be the unique code of the supplier.",
                "One column MUST be 'name' and be the name of the supplier.",
                "One column COULD be 'address'.",
                "One column COULD be 'phone'.",
                "One column COULD be 'email'.",
                "One column COULD be 'website'.",
            ],
            "examples": [
                "code,name,address,phone,email,website",
                "COD1,first supplier,,,first@supplier.com,https://first.supplier.com/",
                "COD2,second supplier,,,second@supplier.com,https://second.supplier.com/",
                "COD3,third supplier,,,third@supplier.com,https://third.supplier.com/",
            ],
        }
        if len(g.errors) == 0 and reader is not None:
            for row in reader:
                supplier = SupplierModel.query.filter_by(
                    code=row[headers["code"]],
                ).first()
                if supplier is None:
                    supplier = SupplierModel(
                        code = row[headers["code"]],
                        name = row[headers["name"]],
                        address = row[headers["address"]],
                        phone = row[headers["phone"]],
                        email = row[headers["email"]],
                        website = row[headers["website"]],
                    )
                    g.messages.append(f"{row[headers['name']]} added.")
                    supplier.save()
                else:
                    updated = False
                    if supplier.name != row[headers["name"]]:
                        supplier.name = row[headers["name"]]
                        updated = True
                    if supplier.address != row[headers["address"]]:
                        supplier.address = row[headers["address"]]
                        updated = True
                    if supplier.phone != row[headers["phone"]]:
                        supplier.phone = row[headers["phone"]]
                        updated = True
                    if supplier.email != row[headers["email"]]:
                        supplier.email = row[headers["email"]]
                        updated = True
                    if supplier.website != row[headers["website"]]:
                        supplier.website = row[headers["website"]]
                        updated = True
                    if updated:
                        g.messages.append(f"{row[headers['name']]} updated.")
                        supplier.save()

        return self.render("admin/import.html")
