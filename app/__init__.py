# encoding: utf-8

from flask_admin import Admin
from flask_babel import Babel
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

admin = Admin()
api = Api()
babel = Babel()
db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()
