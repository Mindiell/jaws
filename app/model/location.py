# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class LocationModel(db.Model, Model):
    __tablename__ = "location"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    parent_id = db.Column(db.Integer, db.ForeignKey("location.id"))
    parent = db.relationship(
        "LocationModel",
        backref=db.backref("children", lazy="dynamic", order_by="LocationModel.left_index"),
        remote_side=[id],
    )
    level = db.Column(db.Integer, default=0)
    left_index = db.Column(db.Integer, default=0)
    right_index = db.Column(db.Integer, default=0)

    def __repr__(self):
        return f"{self.name}" if self.parent_id is None else f"{self.parent}/{self.name}"


class AdminView(View):
    can_export = True
    form_excluded_columns = ["children"]
    form_widget_args = {
        "name": {"style": "width: 600px"},
        "parent": {"style": "width: 600px"},
    }

admin.add_view(AdminView(LocationModel, db.session, name="Location"))


def delete_branch(left_index, right_index):
    """
    Delete a branch from the locations tree.
    """
    # Delete the branch itself
    LocationModel.query.filter(LocationModel.left_index>=left_index).filter(LocationModel.right_index<=right_index).delete()
    # Recompute indexes
    diff = right_index - left_index + 1
    LocationModel.query.filter(LocationModel.left_index>left_index).update({LocationModel.left_index:LocationModel.left_index-diff})
    LocationModel.query.filter(LocationModel.right_index>right_index).update({LocationModel.right_index:LocationModel.right_index-diff})
    db.session.commit()


def insert_branch(branch, previous):
    """
    Insert a branch into the locations tree.
    Branch is 0-indexed.
    branch = [
        {
            "name": "location",
            "parent_id": null,
            "level": 0,
            "left_index": 0,
            "right_index": 1,
        },
    ]
    TODO: Replace previous by an ID (or None)
    TODO: Take None into account as the parent
    """
    # Recompute indexes of the branch
    for part in branch:
        part["left_index"] += previous.left_index + 1
        part["right_index"] += previous.left_index + 1
        part["level"] += previous.level + 1
    # Recompute indexes of the tree
    diff = branch[0]["right_index"] - branch[0]["left_index"] + 1
    LocationModel.query.filter(LocationModel.left_index>previous.left_index).update({LocationModel.left_index:LocationModel.left_index+diff})
    LocationModel.query.filter(LocationModel.right_index>previous.left_index).update({LocationModel.right_index:LocationModel.right_index+diff})
    # Insert each part of the branch
    for part in branch:
        location = LocationModel(
            name=part["name"],
            parent_id=part["parent_id"],
            level=part["level"],
            left_index=part["left_index"],
            right_index=part["right_index"],
        )
        location.save()
    return location
