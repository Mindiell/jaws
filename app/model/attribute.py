# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class AttributeModel(db.Model, Model):
    __tablename__ = "attribute"
    id = db.Column(db.Integer, primary_key=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey("equipment.id"))
    equipment = db.relationship("EquipmentModel", backref=db.backref("attributes", lazy="dynamic"))
    name = db.Column(db.String(200))
    value = db.Column(db.String(200))

    def __repr__(self):
        return self.name


admin.add_view(View(AttributeModel, db.session, name="Attribute"))
