# encoding: utf-8

import bcrypt

from flask import current_app

from app import admin, db
from app.model.model import Model, View


def get_user(user_id):
    return UserModel.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(500), unique=True)
    password_hash = db.Column(db.String(128))
    email = db.Column(db.String(500), unique=True)
    active = db.Column(db.Boolean)
    admin = db.Column(db.Boolean)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode("utf-8"),
            bcrypt.gensalt(rounds=current_app.config["BCRYPT_ROUNDS"]),
        )

    def check_password(self, password):
        return bcrypt.checkpw(password.encode("utf-8"), self.password_hash)

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    def get_id(self):
        return str(self.id)


class AdminView(View):
    column_default_sort = "login"
    column_exclude_list = ("password_hash",)

    def on_model_change(self, form, model, is_created):
        if len(form.password_hash.data) < 128:
            model.password = form.password_hash.data


admin.add_view(AdminView(UserModel, db.session, name="User"))
