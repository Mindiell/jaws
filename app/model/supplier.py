# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class SupplierModel(db.Model, Model):
    __tablename__ = "supplier"
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(20))
    name = db.Column(db.String(200))
    address = db.Column(db.Text)
    phone = db.Column(db.String(20))
    email = db.Column(db.String(200))
    website = db.Column(db.String(200))

    def __repr__(self):
        return self.name


class AdminView(View):
    can_export = True
    column_exclude_list = ("address",)


admin.add_view(AdminView(SupplierModel, db.session, name="Supplier"))
