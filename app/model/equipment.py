# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class EquipmentModel(db.Model, Model):
    __tablename__ = "equipment"
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(20))
    short_name = db.Column(db.String(200))
    long_name = db.Column(db.String(2000))

    @property
    def quantity(self):
        return sum([asset.quantity for asset in self.assets.all()])

    def __repr__(self):
        return self.short_name


class AdminView(View):
    can_export = True
    form_widget_args = {
        "short_name": {"style": "width: 600px"},
        "long_name": {"style": "width: 600px"},
    }


admin.add_view(AdminView(EquipmentModel, db.session, name="Equipment"))
