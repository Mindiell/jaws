# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class AssetModel(db.Model, Model):
    __tablename__ = "asset"
    id = db.Column(db.Integer, primary_key=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey("equipment.id"))
    equipment = db.relationship("EquipmentModel", backref=db.backref("assets", lazy="dynamic"))
    location_id = db.Column(db.Integer, db.ForeignKey("location.id"))
    location = db.relationship("LocationModel", backref=db.backref("assets", lazy="dynamic"))
    quantity = db.Column(db.Integer)

    def __repr__(self):
        return f"{self.equipment.short_name} ({self.quantity})"


admin.add_view(View(AssetModel, db.session, name="Asset"))
