# encoding: utf-8

from datetime import datetime

from flask import redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user

from app import db


class View(ModelView):
    def is_accessible(self):
        # TODO: develop mode
        return True
        if not current_user.is_authenticated:
            return False
        return current_user.is_authenticated and current_user.admin

    def inaccessible_callback(self, name, **kwargs):
        # TODO: develop mode
        return redirect(url_for("core.login"))
        return redirect(url_for("admin.login", next=request.url))


class Model:
    def save(self):
        is_existing = True
        if len(self.__table__.primary_key.columns.keys()) > 1:
            is_existing = False
        else:
            for column_name in self.__table__.primary_key.columns.keys():
                column = self.__getattribute__(column_name)
                if column is None or column == "":
                    is_existing = False
        if not is_existing:
            db.session.add(self)
        db.session.commit()

    def delete(self):
        is_existing = True
        for column_name in self.__table__.primary_key.columns.keys():
            column = self.__getattribute__(column_name)
            if column is None or column == "":
                is_existing = False
        if is_existing:
            db.session.delete(self)
            db.session.commit()

    def serialize(self):
        result_dict = {}
        for key in self.__table__.columns.keys():
            if not key.startswith("_"):
                if isinstance(getattr(self, key), datetime):
                    result_dict[key] = getattr(self, key).strftime("%Y-%m-%d")
                else:
                    result_dict[key] = getattr(self, key)
        return result_dict
