# encoding: utf-8
"""
This module imports models to allow alembic and flask to find them.
"""

from app.model.user import UserModel
from app.model.equipment import EquipmentModel
from app.model.attribute import AttributeModel
from app.model.counter import CounterModel
from app.model.location import LocationModel
from app.model.asset import AssetModel
from app.model.supplier import SupplierModel
