# encoding: utf-8

from app import admin
from app.controller.admin import admin_routes
from app.controller.core import Core
from app.controller.equipment import Equipment
from app.controller.location import Location


# Adding admin endpoints
for route in admin_routes:
    admin.add_view(route[0](name=route[1], endpoint=route[2], category=route[3]))

# Listing normal endpoints
routes = [
    ("/", Core.as_view("home")),
    ("/login", Core.as_view("login")),
    ("/test", Core.as_view("test")),
    ("/location", Location.as_view("index"), ["GET", "POST"]),
    ("/location/<int:location_id>", Location.as_view("edit"), ["GET", "PUT", "DELETE"]),
    ("/equipment", Equipment.as_view("index"), ["GET", "POST"]),
    ("/equipment/<int:equipment_id>", Equipment.as_view("edit"), ["GET", "PUT", "DELETE"]),
]

# Listing API endpoints
apis = []
