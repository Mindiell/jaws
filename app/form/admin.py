# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import FileField, SelectField
from wtforms.validators import DataRequired


class ImportForm(FlaskForm):
    filename = FileField(_("File to import"), validators=[DataRequired()])
