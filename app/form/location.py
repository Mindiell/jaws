# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField
from wtforms.validators import DataRequired


class LocationNewForm(FlaskForm):
    parent = SelectField(_("Parent"), coerce=int)
    name = StringField(_("Name"), validators=[DataRequired()])
