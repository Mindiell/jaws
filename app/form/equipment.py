# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField
from wtforms.validators import DataRequired


class EquipmentNewForm(FlaskForm):
    code = StringField(_("Code"), validators=[DataRequired()])
    short_name = StringField(_("Short Name"), validators=[DataRequired()])
    long_name = StringField(_("Long Name"))
