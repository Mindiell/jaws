# encoding: utf-8
"""
Server module.

Initializes Flask application and extensions and runs it.
"""

import flask

from app import admin, api, babel, db, login_manager, migrate
from app.model import *
from app.routes import apis, routes
from command import commands


application = flask.Flask(__name__, template_folder="app/view")
application.config.from_object("settings")

if "JINJA_ENV" in application.config:
    application.jinja_env.trim_blocks = application.config["JINJA_ENV"]["TRIM_BLOCKS"]
    application.jinja_env.lstrip_blocks = application.config["JINJA_ENV"]["LSTRIP_BLOCKS"]

# Loading routes
for route in routes:
    if len(route) < 3:
        application.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        application.add_url_rule(route[0], route[1].__name__, route[1], methods=route[2])
# Loading API routes
for route in apis:
    api.add_resource(route[1], route[0])

# Initialisation of extensions
admin.init_app(application)
api.init_app(application)
babel.init_app(application)
db.init_app(application)
login_manager.init_app(application)
migrate.init_app(application, db)

# Manage commands
for command in commands:
    application.cli.add_command(command)


# Manage locale
@babel.localeselector
def get_locale():
    """
    Return locale for flask-babel extension.
    """
    return flask.session.get("locale", flask.current_app.config["BABEL_DEFAULT_LOCALE"])


# Manage user
@login_manager.user_loader
def load_user(user_id):
    """
    Load user from its id.
    """
    return get_user(user_id)