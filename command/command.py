# encoding: utf-8

import sys

import click
from flask.cli import with_appcontext


@click.group()
@click.pass_context
def command(context):
    "Tools for application"
    context.ensure_object(dict)


@command.command(help="Initialize application")
@click.pass_context
@with_appcontext
def init(context):
    click.echo("Initializing application")
